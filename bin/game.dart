import 'dart:io';
import 'dart:math';

/*
Score : 
Walk = 1 per tile
Get item = 5
Attack = 1 per damage
Kill a monster = 20
Get to the finish = 50
*/
void main(List<String> arguments) {
  Game game = Game();
  game.showWelcome();
  game.startGame();
  while (!game.isFinished) {
    game.process();
    if (!game.playerHasHP()) {
      printContinue(game);
    } else {
      game.printLine();
      game.map.showMap();
    }
    if (game.checkWin()) {
      print("Congratulations you win!!!");
      printContinue(game);
    }
  }
}

void printContinue(Game game) {
  print("Your score : ${game.player.score}");
  print("Total score : ${game.player.totalScore}");
  print("Do you want to continue?");
  print("(Y)Yes (N)No");
  while (true) {
    String input = stdin.readLineSync()!;
    if (input == "Y") {
      print("Starting new game...");
      game.player.totalScore += game.player.score;
      game.newGame();
      break;
    } else if (input == "N") {
      print("Exit game.");
      print("Thanks for playing!!!");
      exit(0);
    }
  }
}

class Game {
  Map map = Map();
  Player player = Player();
  late Monster monster;
  bool isFinished = false;
  String turn = "Player";
  late String input;

  void startGame() {
    player.createCharacter();
    printLine();
    map.showMap();
  }

  void newGame() {
    map = Map();
    player.inventory = [Item("HP"), Item(""), Item(""), Item("")];
    isFinished = false;
    player.index = 0;
    player.hp = player.maxHP;
    player.score = 0;
    printLine();
    map.showMap();
  }

  void showWelcome() {
    print("Welcome to game.");
  }

  void process() {
    showAction();
    getInput();
    if (input == "R") {
      int rand = Random().nextInt(3) + 1;
      print("You roll $rand");
      player.score += rand;
      player.walk(rand);
      if (map.inMap(player)) {
        if (map.tileType(player.index) == "Monster") {
          print("You encounter a monster!!!");
          sleep(Duration(seconds: 1));
          monster = Monster();
          turn = "Player";
          enterFight();
        } else if (map.tileType(player.index) == "Item") {
          int count = 0;
          for (int i = 0; i < player.inventory.length; i++) {
            if (player.inventory[i].itemName == "-") {
              player.inventory[i] = Item("HP");
              print("You got HP Potion");
              player.score += 5;
              break;
            } else {
              count++;
            }
          }
          if (count == 4) {
            print("Your inventory is full.");
          }
        }
        map.setPlayer(player.index, rand);
      } else {
        map.setPlayerWin(player.index, rand);
      }
    } else if (input == "I") {
      printLine();
      player.showInventory();
      chooseItem();
    } else if (input == "S") {
      printLine();
      player.showStat();
      player.showWinLose();
      player.showScore();
      print("(B)Back");
      do {
        getInput();
      } while (input != "B");
    } else if (input == "Q") {
      print("Exit game.");
      print("Thanks for playing!!!");
      exit(0);
    }
  }

  void enterFight() {
    do {
      printLine();
      showStats(player, monster);
      if (turn == "Player") {
        print("Your turn");
        showOptionAttack();
        getInput();
        if (input == "R") {
          int playerRoll = Random().nextInt(6) + 1 + player.atk;
          int monsterRoll = Random().nextInt(6) + 1;
          print(
              "You roll ${playerRoll - player.atk} (Attack ${checkGreaterThanZero(playerRoll)})");
          playerRoll = checkGreaterThanZero(playerRoll);
          sleep(Duration(seconds: 1));
          if (monster.evade >= playerRoll ||
              monster.hp - (playerRoll - (monster.def + 6)) <= 0 ||
              monster.hp == 1 ||
              playerRoll == 1) {
            monsterRoll += monster.evade;
            print(
                "Monster rolls ${monsterRoll - monster.evade} (Evade ${checkGreaterThanZero(monsterRoll)})");
            monsterRoll = checkGreaterThanZero(monsterRoll);
            sleep(Duration(seconds: 1));
            if (playerRoll >= monsterRoll) {
              print("You deal $playerRoll damage");
              player.score += playerRoll;
              player.totalScore += playerRoll;
              monster.hp -= playerRoll;
              sleep(Duration(seconds: 1));
            } else {
              print("The monster evaded");
              sleep(Duration(seconds: 1));
            }
          } else {
            monsterRoll += monster.def;
            print(
                "Monster rolls ${monsterRoll - monster.def} (Defend ${checkGreaterThanZero(monsterRoll)})");
            monsterRoll = checkGreaterThanZero(monsterRoll);
            sleep(Duration(seconds: 1));
            if (playerRoll <= monsterRoll) {
              print("You deal 1 damage");
              player.score++;
              player.totalScore++;
              monster.hp -= 1;
              sleep(Duration(seconds: 1));
            } else {
              print("You deal ${playerRoll - monsterRoll} damage");
              player.score += (playerRoll - monsterRoll);
              player.totalScore += (playerRoll - monsterRoll);
              monster.hp -= (playerRoll - monsterRoll);
              sleep(Duration(seconds: 1));
            }
          }
        } else if (input == "I") {
          player.showInventory();
          bool checkInput = false;
          do {
            getInput();
            switch (input) {
              case "1":
                if (player.inventory[0].itemName == "-") {
                  switchTurn(); //Stay in player's turn
                } else if (player.inventory[0].itemName == "HP Potion") {
                  if (player.hp != player.maxHP) {
                    player.useItem(0);
                  } else {
                    player.useItem(0);
                    switchTurn(); //Stay in player's turn
                  }
                }
                checkInput = true;
                break;
              case "2":
                if (player.inventory[1].itemName == "-") {
                  switchTurn(); //Stay in player's turn
                } else if (player.inventory[1].itemName == "HP Potion") {
                  if (player.hp != player.maxHP) {
                    player.useItem(1);
                  } else {
                    player.useItem(1);
                    switchTurn(); //Stay in player's turn
                  }
                }
                checkInput = true;
                break;
              case "3":
                if (player.inventory[2].itemName == "-") {
                  switchTurn(); //Stay in player's turn
                } else if (player.inventory[2].itemName == "HP Potion") {
                  if (player.hp != player.maxHP) {
                    player.useItem(2);
                  } else {
                    player.useItem(2);
                    switchTurn(); //Stay in player's turn
                  }
                }
                checkInput = true;
                break;
              case "4":
                if (player.inventory[3].itemName == "-") {
                  switchTurn(); //Stay in player's turn
                } else if (player.inventory[3].itemName == "HP Potion") {
                  if (player.hp != player.maxHP) {
                    player.useItem(3);
                  } else {
                    player.useItem(3);
                    switchTurn(); //Stay in player's turn
                  }
                }
                checkInput = true;
                break;
              case "B":
                switchTurn(); //Stay in player's turn
                checkInput = true;
                break;
              default:
                continue;
            }
            sleep(Duration(seconds: 1));
          } while (!checkInput);
        } else {
          continue;
        }
      } else {
        int monsterRoll = Random().nextInt(6) + 1 + monster.atk;
        print(
            "Monster roll ${monsterRoll - monster.atk} (Attack ${checkGreaterThanZero(monsterRoll)})");
        monsterRoll = checkGreaterThanZero(monsterRoll);
        sleep(Duration(seconds: 1));
        showOptionDefense();
        while (true) {
          getInput();
          if (input == "D") {
            int playerRoll = Random().nextInt(6) + 1 + player.def;
            print(
                "You roll ${playerRoll - player.def} (Defend ${checkGreaterThanZero(playerRoll)})");
            playerRoll = checkGreaterThanZero(playerRoll);
            sleep(Duration(seconds: 1));
            if (playerRoll >= monsterRoll) {
              print("You take 1 damage");
              player.hp -= 1;
            } else {
              print("You take ${monsterRoll - playerRoll} damage");
              player.hp -= (monsterRoll - playerRoll);
            }
            sleep(Duration(seconds: 1));
            break;
          } else if (input == "E") {
            int playerRoll = Random().nextInt(6) + 1 + player.evade;
            print(
                "You roll ${playerRoll - player.evade} (Evade ${checkGreaterThanZero(playerRoll)})");
            checkGreaterThanZero(playerRoll);
            sleep(Duration(seconds: 1));
            if (playerRoll > monsterRoll) {
              print("You evaded");
            } else {
              print("You take $monsterRoll damage");
              player.hp -= monsterRoll;
            }
            sleep(Duration(seconds: 1));
            break;
          }
        }
      }
      if (monster.hp <= 0) {
        print("You killed the monster!!!");
        player.score += 20;
        break;
      }
      switchTurn();
    } while (monster.hp > 0 && player.hp > 0);
  }

  void chooseItem() {
    bool checkInput = false;
    do {
      getInput();
      switch (input) {
        case "1":
          if (player.inventory[0] != "") {
            player.useItem(0);
          }
          checkInput = true;
          break;
        case "2":
          if (player.inventory[1] != "") {
            player.useItem(1);
          }
          checkInput = true;
          break;
        case "3":
          if (player.inventory[2] != "") {
            player.useItem(2);
          }
          checkInput = true;
          break;
        case "4":
          if (player.inventory[3] != "") {
            player.useItem(3);
          }
          checkInput = true;
          break;
        case "B":
          checkInput = true;
          break;
        default:
          continue;
      }
    } while (!checkInput);
  }

  void showOptionAttack() {
    print("(R)Roll Attack (I)Use Item");
  }

  void showOptionDefense() {
    print("(D)Defend (E)Evade");
  }

  void getInput() {
    input = stdin.readLineSync()!;
  }

  void showAction() {
    print("Choose your action:");
    print("(R)Roll Walk[1-3] (I)Use item (S)Show stat (Q)Quit game");
  }

  bool checkWin() {
    if (player.index >= 29) {
      player.win++;
      isFinished = true;
      player.score += 50;
      return true;
    }
    return false;
  }

  void showStats(Player player, Monster monster) {
    player.showStat();
    print("");
    monster.showStat();
    print("");
  }

  void switchTurn() {
    if (turn == "Player")
      turn = "Monster";
    else
      turn = "Player";
  }

  bool playerHasHP() {
    if (player.hp > 0) {
      return true;
    }
    player.lose++;
    print("Game over!!!");
    print("You died.");
    isFinished = true;
    return false;
  }

  void printLine() {
    print("--------------------------------------------------------------");
  }

  int checkGreaterThanZero(int Roll) {
    if (Roll <= 0) {
      return 1;
    }
    return Roll;
  }
}

class Map {
  List<String> map = [">>>Player<<<"];

  Map() {
    for (int i = 1; i < 29; i++) {
      map.add(randomTile());
    }
    map.add("Goal");
  }

  void showMap() {
    print("Map:");
    print(map);
  }

  String randomTile() {
    int rand = Random().nextInt(3) + 1;
    switch (rand) {
      case 1:
        return "-";
      case 2:
        return "Item";
      case 3:
        return "Monster";
      default:
        return "-";
    }
  }

  void setPlayer(int index, int lastindex) {
    map[index - lastindex] = "-";
    map[index] = ">>Player<<";
  }

  bool inMap(Player player) {
    return player.index <= 29;
  }

  void setPlayerWin(int index, int rand) {
    map[index - rand] = "-";
    map[map.length - 1] = ">>>Player<<<";
  }

  String tileType(int index) {
    return map[index];
  }
}

class Player {
  late String name;
  String playerClass = "";
  late int maxHP;
  late int hp;
  late int atk;
  late int def;
  late int evade;
  late String inputClass;
  int totalScore = 0;
  int score = 0;
  int win = 0;
  int lose = 0;
  int index = 0;
  List<Item> inventory = [Item("HP"), Item(""), Item(""), Item("")];

  void createCharacter() {
    print("Enter your name: ");
    name = stdin.readLineSync()!;
    print("");
    while (playerClass.isEmpty) {
      print("Choose your class:");
      print("(1)Standard (HP = 6 ATK = 0 DEF = 0 EVA = 0)");
      print("(2)Swordman (HP = 8 ATK = 2 DEF = -1 EVA = 0)");
      print("(3)Knight (HP = 10 ATK = 0 DEF = 2 EVA = -2)");
      print("(4)Theif (HP = 5 ATK = 1 DEF = -2 EVA = 3)");
      inputClass = stdin.readLineSync()!;
      if (inputClass == "1") {
        playerClass = "Standard";
        maxHP = 6;
        hp = 6;
        atk = 0;
        def = 0;
        evade = 0;
      } else if (inputClass == "2") {
        playerClass = "Swordman";
        maxHP = 8;
        hp = 8;
        atk = 2;
        def = -1;
        evade = 0;
      } else if (inputClass == "3") {
        playerClass = "Knight";
        maxHP = 10;
        hp = 10;
        atk = 0;
        def = 2;
        evade = -2;
      } else if (inputClass == "4") {
        playerClass = "Theif";
        maxHP = 5;
        hp = 5;
        atk = 1;
        def = -2;
        evade = 3;
      }
    }
    print("Welcome $name!");
    print("Your class is : $playerClass.");
    sleep(Duration(seconds: 1));
  }

  void showInventory() {
    print("Your inventory:");
    print("(1) ${inventory[0]}");
    print("(2) ${inventory[1]}");
    print("(3) ${inventory[2]}");
    print("(4) ${inventory[3]}");
    print("(B)Back");
  }

  void showStat() {
    if (hp == maxHP) {
      print("Name: $name");
      print("Class: $playerClass");
      print("HP: $hp   ATK: $atk");
      print("DEF: $def   EVA: $evade");
    } else {
      print("Name: $name");
      print("Class: $playerClass");
      print("HP: $hp/$maxHP   ATK: $atk");
      print("DEF: $def   EVA: $evade");
    }
  }

  void useItem(int index) {
    if (inventory[index].itemName == "HP Potion") {
      if (hp < maxHP) {
        hp = maxHP;
        inventory[index] = Item("");
        print("You healed back to full HP");
      } else {
        print("You are already full HP");
      }
    }
  }

  void walk(int step) {
    index += step;
  }

  void showWinLose() {
    print("Win : $win");
    print("Lose : $lose");
  }

  void showScore() {
    print("Score : $score");
    print("Total score : ${score + totalScore}");
  }
}

class Monster extends Player {
  Monster() {
    playerClass = "Monster";
    hp = Random().nextInt(3) + 6;
    atk = Random().nextInt(5) - 2;
    if (atk > 0) {
      def = Random().nextInt(3) - 2;
      evade = Random().nextInt(3) - 2;
    } else {
      def = Random().nextInt(3) - 1;
      evade = Random().nextInt(3) - 1;
    }
  }

  void showStat() {
    print("Monster");
    print("HP: $hp   ATK: $atk");
    print("DEF: $def   EVA: $evade");
  }
}

class Item {
  String itemName = "-";
  String type = "-";
  String description = "-";

  Item(String name) {
    if (name == "HP") {
      itemName = "HP Potion";
      type = "heal";
      description = "Used to heal yourself back to full hp.";
    }
  }

  String toString() {
    return itemName;
  }
}
